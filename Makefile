CC=gcc
CFLAGS=-Im.
DEPS = point.h
OBJ = main.o 
LIBS=-lm

all: title pointmake clean

%.o: %.c $(DEPS)
	$(CC) -c -o $@ $< $(CFLAGS)

pointmake: $(OBJ)
	gcc -o $@ $^ $(CFLAGS) $(LIBS)

.PHONY: clean

title:
	$(info *******CRISTIAN PENA *********)
	$(info *******JOHN CEDENO   *********)
clean:
	rm -f *.o
